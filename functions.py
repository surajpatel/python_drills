def is_prime(n):
    """
    Check whether a number is prime or not
    """
    count = 0
    for i in range(2,int(n/2+1)):
        if n % i == 0:
            count += 1
    if count == 0:
        return True
    else:
        return False

def n_digit_primes(digit):
    """
    Return a list of `n_digit` primes using the `is_prime` function.

    Set the default value of the `digit` argument to 2
    """
    return_n_digit_primes = list()
    start = 10**(digit-1)+1
    end = 10**digit
    for i in range(start,end):
        return_bool = is_prime(i)
        if return_bool == True:
            return_n_digit_primes.append(i)
    return return_n_digit_primes
