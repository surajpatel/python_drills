def last_3_characters(x):
    if len(x)<=3:
        return x
    return_last_3_characters = x[-3:]
    return return_last_3_characters
    


def first_10_characters(x):
    if len(x) <= 10:
        return x
    return_first_10_characters = x[:10]
    return return_first_10_characters

def chars_4_through_10(x):
    if len(x) <= 4:
        return ''
    return_chars_4_through_10 = x[4:11] 
    return return_chars_4_through_10

def str_length(x):
    # return len(x)
    convert_in_list = list(x)
    count = 0
    for i in convert_in_list:
        count = count + 1
    return count
    


def words(x):
    no_of_words = x.split()
    return no_of_words



def capitalize(x):
    if len(x) <= 0:
        return ""
    convert_into_list = x.split()
    if ord(convert_into_list[0][0]) >= 97:
        char = chr(ord(convert_into_list[0][0])-32)
        return_string = char + x[1:]
    return return_string
    # return x.capitalize()

def to_uppercase(x):
    if len(x) <= 0:
        return ""
    char = list(x)
    string = ""
    for i in char:
        if ord(i) >= 97 and ord(i) <= 122:
            string = string + chr(ord(i)-32)
        else:
            string = string + i
    return string
    # return x.upper()


