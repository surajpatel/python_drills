def integers_from_start_to_end_using_range(start, end, step):
    """return a list"""
    return_list = list()
    for i in range(start,end,step):
        return_list.append(i)
    return return_list

def integers_from_start_to_end_using_while(start, end, step):
    """return a list"""
    return_list = list()
    if start < end:
        while(start < end):
            return_list.append(start)
            start += step
    else:
        while(start > end):
            return_list.append(start)
            start += step
    return return_list


def two_digit_primes():
    """
    Return a list of all two-digit-primes
    """
    return_prime_list = list()
    for i in range(10,100):
        count = 0
        for j in range(2,int(i/2+1)):
            if i%j == 0:
                count += 1
        if count == 0:
            return_prime_list.append(i)
    return return_prime_list
