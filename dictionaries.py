def word_count(s):
    """
    Find the number of occurrences of each word
    in a string(Don't consider punctuation characters)
    """
    words_list = s.split()
    clear_string = str()
    for i in words_list:
        clear_string += " " + i.strip(",").strip(".")

    return_word_count = dict()
    words_list = clear_string.split()
    for i in words_list:
        if i in return_word_count:
            return_word_count[i] = return_word_count[i] + 1
        else:
            return_word_count[i] = 1

    return return_word_count

def dict_items(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    """
    if len(d) <= 0:
        return []
    return_dict_items = list()
    for k, v in d.items():
        empty_tuple = (k,v)
        return_dict_items.append(empty_tuple)
    return return_dict_items


def dict_items_sorted(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    sorted by `d`'s keys
    """
    if len(d) <=0:
        return []
    return_dict_items_sorted = list()
    list_key = sorted(d)
    for i in list_key:
        return_dict_items_sorted.append((i,d[i]))
    return return_dict_items_sorted

