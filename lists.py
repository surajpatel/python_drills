def sum_items_in_list(x):
    if len(x) <= 0:
        return 0
    sum = 0
    for i in x:
        sum += i
    return sum


def list_length(x):
    # return len(x)
    count = 0
    for i in x:
        count += 1
    return count

def last_three_items(x):
    if len(x) <= 3:
        return x
    return x[-3:]

def first_three_items(x):
    if len(x) <= 3:
        return x
    return x[:3]

def sort_list(x):
    x.sort()
    return x

def append_item(x, item):
    x.append(item)
    return x

def remove_last_item(x):
    x.pop()
    return x


def count_occurrences(x, item):
    return x.count(item)

def is_item_present_in_list(x, item):
    # if x.count(item) > 0:
    #     return True
    # return False
    try:
        x.index(item)
        return True
    except ValueError:
        return False


def append_all_items_of_y_to_x(x, y):
    """
    x and y are lists
    """
    for i in y:
        x.append(i)
    return x


def list_copy(x):
    """
    Create a shallow copy of x
    """
    return x.copy()


