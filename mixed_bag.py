def unique(items):
    """
    Return a set of unique values belonging to the `items` list
    """
    if len(items) <= 0:
        return set()
    s = set(items)
    insert_list = []
    for i in s:
        insert_list.append(i)
    return set(insert_list)

import random
def shuffle(items):
    """
    Shuffle all items in a list
    """
    random.shuffle(items)
    return items

# import sys
import os
def getcwd():
    """
    Get current working directory
    """
    return os.getcwd()


def mkdir(name):
    """
    Create a directory at the current working directory
    """
    if not os.path.exists(name):
        os.makedirs(name)

