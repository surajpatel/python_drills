"""
Implement the below file operations.
Close the file after each operation.
"""


def read_file(path):
    with open(path,"r+") as f:
        data = f.read()
    return data
    f.close()

def write_to_file(path, s):
    with open(path,"r+") as f:
        write_data = f.write(s)
    f.close()
def append_to_file(path, s):
    with open(path, 'a+') as f:
        f.write(s)
    f.close()

def numbers_and_squares(n, file_path):
    """
    Save the first `n` natural numbers and their squares into a file in the csv format.

    Example file content for `n=3`:

    1,1
    2,4
    3,9
    """
    with open(file_path,"w+") as f:
        for i in range(1,n+1):
            st = str(i)+','+str(i*i)+"\n"
            f.write(st)
        print(f.read())
    f.close()
    
